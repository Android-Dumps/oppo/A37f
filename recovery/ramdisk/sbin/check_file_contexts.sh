#!/bin/sh
#check_file_contexts zip_path
zip_path="$1"
echo "input is [$zip_path]"
new_ctx=/tmp/file_contexts
cur_ctx=/tmp/file_contexts0
busybox unzip -p "$zip_path" META-INF/com/android/file_contexts | busybox sort > $new_ctx

if [[ -s $new_ctx ]] ; then 
    cat /file_contexts | sort > $cur_ctx 
    echo "new ctx file has data.";
    con_diff=`busybox diff $new_ctx $cur_ctx | busybox grep -E "^[+-]/data"`
    echo "diff is $con_diff"
	
    if [ ! $con_diff ] ;then 
        echo "no change in file_contexts"
    else
        echo "shoud do restorecon"
        busybox unzip -p "$zip_path" /file_contexts 
        restorecon -RF /data 
    fi
	
else 
    echo "new ctx file is empty.";
fi ;
